<?php

class LoginController extends \BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
//        $d= new User();
//        $d->email= 'admin1@admin.com';
//        $d->password= Hash::make('password');
//        $d->save();

        if(Auth::check()) return Redirect::to('admin');
		return View::make('login');

	}

    public function checkLogin()
    {
        $email= Request::get('email');
        $password= Request::get('password');

        if(Auth::attempt(['email'=>$email,'password'=>$password],true)){
            return Redirect::to('admin');
        }else{
            return Redirect::to('login');
        }
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


    public function logout(){
        Auth::logout();
        return Redirect::to('login');
    }


}
