<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/','LoginController@index');
Route::get('login','LoginController@index');
Route::get('logout','LoginController@logout');
Route::post('postRemind','RemindersController@postRemind');
Route::get('getReset/{token}','RemindersController@getReset');
Route::post('postReset','RemindersController@postReset');
Route::post('login','LoginController@checkLogin');
Route::group(['before'=>'auth'],function(){
    Route::get('admin','AdminController@index');
});
