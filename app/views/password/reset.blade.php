<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Password Reset Panel</title>
    {{ HTML::style('assets/css/bootstrap.min.css') }}
</head>
<body style="background-color: #e5e5e5">
<div class="container">
    <div class="row">
        <div class="col-md-offset-4 col-md-4">
            <div class="login" style="position: relative;min-height: 650px;">
                <div style="position: absolute;width: 100%;top: 50%;margin-top: -180px">
                    <h3 class="text-center" style="background: #080;padding: 5px;color: white;border-radius: 10px 10px 0px 0px;">School Management System</h3>

                    <div class="panel panel-default">
                        <div class="panel-heading">Password Reset Panel</div>
                        <div class="panel-body">
                            <?php
                            $message=Session::get('error');
                            ?>
                            @if(isset($message))
                                {{ $message }}
                            @endif
                            {{ Form::open(['url'=>'postReset']) }}
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" name="password_confirmation" class="form-control">
                                </div>
                                <input type="submit" value="Reset Password" class="btn btn-primary">
                                <a href="{{ URL::to('login') }}" class="btn btn-danger">Back</a>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>