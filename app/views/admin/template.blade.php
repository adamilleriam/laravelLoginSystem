<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.template.head')
</head>

<body>
@include('admin.template.top_nav')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            @include('admin.template.left_nav')
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            @yield('content')
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
</body>
</html>
