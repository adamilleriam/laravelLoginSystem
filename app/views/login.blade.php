<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Login Panel</title>
    {{ HTML::style('assets/css/bootstrap.min.css') }}
    {{ HTML::script('assets/js/jquery-2.1.4.min.js') }}
    {{ HTML::script('assets/js/bootstrap.min.js') }}
</head>
<body style="background-color: #e5e5e5">
<div class="container">
    <div class="row">
        <div class="col-md-offset-4 col-md-4">
            <div class="login" style="position: relative;min-height: 650px;">
                <div style="position: absolute;width: 100%;top: 50%;margin-top: -200px">
                    <h3 class="text-center" style="background: #080;padding: 5px;color: white;border-radius: 10px 10px 0px 0px;">School Management System</h3>

                    <div class="panel panel-default">
                        <div class="panel-heading">Login Panel</div>
                        <div class="panel-body">
                            <?php
                                $message=Session::get('status');
                            ?>
                            @if(isset($message))
                                {{ $message }}
                            @endif
                            <?php
                                $message=Session::get('error');
                            ?>
                            @if(isset($message))
                                {{ $message }}
                            @endif
                            <div id="loginDiv">
                                {{ Form::open(['url'=>'login']) }}
                                <div class="form-group">
                                    {{ Form::label('email','Email'); }}
                                    {{ Form::email('email','',['class'=>'form-control']); }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('password','Password'); }}
                                    {{ Form::password('password',['class'=>'form-control']); }}
                                </div>
                                <div class="form-group">
                                    <label>
                                        {{ Form::Checkbox('remember_me','',['class'=>'form-control']) }}
                                        Remember Me
                                    </label>
                                </div>
                                {{ Form::submit('LogIn',['class'=>'btn btn-primary btn-block']) }}
                            </div>
                            <a href="#" id="forgotBtn">Forgot password ?</a>
                            <div style="display: none" id="forgotDiv">
                                <h4>Forgot Password</h4>
                                {{ Form::close() }}
                                {{ Form::open(['url'=>'postRemind']) }}
                                {{--<form action="{{ postRemind }}" method="POST">--}}
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Enter your email">
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <input type="submit" value="Send Reminder" class="btn btn-primary">
                                    </div>
                                    <div class="col-md-4"><a href="#" id="back" class="btn btn-danger pull-right">Back</a></div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    $(function(){
        $('#forgotBtn').click(function(){
            $(this).hide();
            $('#loginDiv').hide();
            $('#forgotDiv').show();
        });
        $('#back').click(function(){
            $('#forgotDiv').hide();
            $('#forgotBtn').show();
            $('#loginDiv').show();
        });
    });
</script>
</html>